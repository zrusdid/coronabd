module.exports = {
  "globDirectory": "build/",
  "globPatterns": [
    "**/*.{html,eot,svg,ttf,woff,woff2,otf,png,txt,jpg,gif,php,md,js,css}"
  ],
  "swDest": "build/sw.js",
  "swSrc": "src/sw.js"
};